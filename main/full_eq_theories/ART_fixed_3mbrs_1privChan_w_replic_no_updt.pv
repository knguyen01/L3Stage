(*Diffie-Hellman relation*)
type G.
const g:G [data].
type exponent.
const one:exponent. (*a dummy sec. key for a missing node in the tree*)

fun exp(G, exponent):G.
equation forall x:exponent, y:exponent;
	exp(exp(g, x), y) = exp(exp(g, y), x).


(*set preciseActions = true.*)
set verboseClauses = short.

(*set verboseRules = true.*)


(*Public-key encryption and signature*)
type pkey.
type skey.
const OK:bitstring [data].

fun pk(skey):pkey.
fun aenc(bitstring, pkey):bitstring.

reduc forall m:bitstring, sk:skey;
	adec(aenc(m, pk(sk)), sk) = m.

fun sign(bitstring, skey):bitstring.

reduc forall m:bitstring, sk:skey;
	getmsg(sign(m, sk)) = m.

reduc forall m:bitstring, sk:skey;
	checksign(sign(m , sk), m, pk(sk)) = OK.

(*Mac and symmetric keys*)
type stage_key.
const sk_0:stage_key. (*starting value for stage key, before init.*)
fun senc(bitstring, stage_key):bitstring.

reduc forall m:bitstring, k:stage_key;
	sdec(senc(m, k), k) = m.

fun smac(bitstring, stage_key):bitstring.


fun KeyExchange(skey, pkey, skey, pkey):exponent. (*1-round key exchange function*)

equation forall ia:skey, ib:skey, ea:skey, eb:skey;
	KeyExchange(ia, pk(ib), ea, pk(eb)) = KeyExchange(ib, pk(ia), eb, pk(ea)).

fun i(G):exponent [data]. (*injection from group elements to integers, modeled as hash func.*)
fun KDF(exponent, stage_key):stage_key. (*KDF takes as input the tk and previous stage key*)

type id.
type s_id.
type ssnid.

event Cell(channel,nat,bitstring).

axiom d:channel, i:nat, s:bitstring, s':bitstring;
	event(Cell(d,i,s)) && event(Cell(d,i,s')) ==> s = s'.

(*the following 3 functions will be used in function
'genHonest' and 'genDishonest', in order to create honest and dishonest
members*)
fun beHonest(s_id):id [private].
fun beDishonest(s_id):id [private].
fun ik(s_id):skey [private]. (*generate a fresh identity key for the input seed id*)

fun ik_of_id(id):skey
reduc
	forall x:s_id; ik_of_id(beHonest(x)) = ik(x)
	otherwise forall x:s_id; ik_of_id(beDishonest(x)) = ik(x) [private] .

fun fst(bitstring):pkey
reduc forall IK:pkey, dumm:bitstring;
	fst((IK, dumm)) = IK [private].


fun is_dishonest(id):bool
reduc forall s:s_id; is_dishonest(beDishonest(s)) = true.

fun is_honest(id):bool
reduc forall s:s_id; is_honest(beHonest(s)) = true.

table keys(id, pkey). (*held by the server to provide the public identity key*)
table ekeys(id, pkey). (*held by the server to provide the public ephemeral key*)

(*We need a table, or a function, mapping from an id to its private channels for tree key,
stage key and leaf key. This func. needs to be private*)
fun cAll(id, ssnid):channel [private]. (*cAll(X, ..) containing the tk, X's leaf_key, stg_k, X's tree view*)

free c:channel.
free s:bitstring [private].

fun bitstring_of_pk(pkey):bitstring[typeConverter].
fun pk_of_bitstring(bitstring):pkey[typeConverter].

let server(skS:skey) =
	!
  in(c, R:id); (*queries for one-time ephemeral keys and identity keys*)
	get keys(=R, IK_R) in (* Does proverif consider several keys for the same R ? *)
	get ekeys(=R, EK_R) in (* Does proverif consider several keys for the same R ? *)
	out(c, (R, (IK_R, sign((R,IK_R, EK_R), skS))) ).

event Test.

(*the function signatures below might change*)
let memInitiate(pkS:pkey, ssionid:ssnid, X:id, Y:id, Z:id) = (*X plays as an initiator w.r.t Y and Z*)
	if is_honest(X)
	then (

		(*Get the public id. key and public ephemeral key*)
		out(c, Y);out(c, Z);
		let ik_X = ik_of_id(X) in
		in(c, (=Y, m_IK_Y:bitstring));
		in(c, (=Z, m_IK_Z:bitstring));

		if checksign(m_IK_Y, getmsg(m_IK_Y), pkS) = OK then
		let (=Y,IK_Y:pkey, EK_Y:pkey) = getmsg(m_IK_Y) in

		if checksign(m_IK_Z, getmsg(m_IK_Z), pkS) = OK then
		let (=Z,IK_Z:pkey, EK_Z:pkey) = getmsg(m_IK_Z) in

    event Test;

		(*do the one-round key exchange to derive leaf keys*)
		new ek_X[]:skey;
		insert ekeys(X, pk(ek_X));
		new leaf_X[]:exponent;
		let leaf_Y = KeyExchange(ik_X, IK_Y, ek_X, EK_Y) in
		let leaf_Z = KeyExchange(ik_X, IK_Z, ek_X, EK_Z) in
		(*get the private channels for variables*)
		let var_all_X = cAll(X, ssionid) in

		let par_XY = i(exp( exp(g, leaf_Y), leaf_X )) in
		let root = i(exp( exp(g, par_XY), leaf_Z )) in
		let stg_k = KDF(root, sk_0) in

		let brdcst_tree = ( (*in-order traversal*)
			exp(g, leaf_X), exp(g, par_XY), exp(g, leaf_Y), exp(g, root), exp(g, leaf_Z)
		) in
		(
		event Cell(var_all_X,0, (root, leaf_X, stg_k, brdcst_tree));
		out(var_all_X, (0,root, leaf_X, stg_k, brdcst_tree));
		out(c, sign(brdcst_tree, ik_X));
		out(c, senc(s, stg_k))
		)
	).

let memTwoInit(pkS:pkey, ssionid:ssnid, X:id, I:id) = (*X plays as a member init. his tree, w.r.t initiator I, whose path length is 2*)
	if is_honest(X)
	then (
		let ik_X = ik_of_id(X) in

		new ek_X[]:skey; insert ekeys(X, pk(ek_X));
		out(c, I); in(c, (=I, m_IK_I:bitstring) );

		if checksign(m_IK_I, getmsg(m_IK_I), pkS) = OK then
		let (=I,IK_I:pkey, EK_I:pkey) = getmsg(m_IK_I) in

		in(c, m:bitstring); (*get the broadcasted tree*)
		if checksign(m, getmsg(m), IK_I) = OK then
		let (LEAF_1:G, PAR_12:G, LEAF_2:G, ROOT:G, LEAF_3:G) = getmsg(m) in
		let leaf_X = KeyExchange(ik_X, IK_I, ek_X, EK_I) in (*derive its leaf key*)

		(*get the channels for variables*)
		let var_all_X = cAll(X, ssionid) in

		let par_X = i(exp(LEAF_1, leaf_X)) in
		let root = i(exp(LEAF_3, par_X)) in
		let stg_k = KDF(root, sk_0) in
		event Cell(var_all_X,0, (root, leaf_X, stg_k, (LEAF_1, exp(g, par_X), exp(g, leaf_X), exp(g, root), LEAF_3)));
		out(var_all_X, (0,root, leaf_X, stg_k, (LEAF_1, exp(g, par_X), exp(g, leaf_X), exp(g, root), LEAF_3)));
		out(c, senc(s, stg_k))
	).

let memThreeInit(pkS:pkey, ssionid:ssnid, X:id, I:id) = (*X plays as a member init. his tree, w.r.t initiator I, whose path length is 1*)
	if is_honest(X)
	then (
		let ik_X = ik_of_id(X) in

		new ek_X[]:skey; insert ekeys(X, pk(ek_X));
		out(c, I); in(c, (=I, m_IK_I:bitstring));
		if checksign(m_IK_I, getmsg(m_IK_I), pkS) = OK then
		let (=I,IK_I:pkey, EK_I:pkey) = getmsg(m_IK_I) in

		in(c, m:bitstring); (*get the broadcasted tree*)
		if checksign(m, getmsg(m), IK_I) = OK then
		let (LEAF_1:G, PAR_12:G, LEAF_2:G, ROOT:G, LEAF_3:G) = getmsg(m) in
		let leaf_X = KeyExchange(ik_X, IK_I, ek_X, EK_I) in (*derive its leaf key*)

		(*get the channels for variables*)
		let var_all_X = cAll(X, ssionid) in

		let root = i(exp(PAR_12, leaf_X)) in
		let stg_k = KDF(root, sk_0) in
		event Cell(var_all_X,0, (root, leaf_X, stg_k, (LEAF_1, PAR_12, LEAF_2, exp(g, root), exp(g, leaf_X))));
		out(var_all_X, (0,root, leaf_X, stg_k, (LEAF_1, PAR_12, LEAF_2, exp(g, root), exp(g, leaf_X))));
		out(c, senc(s, stg_k))
	).

let memUpdate(ssionid:ssnid, X:id, new_leaf:exponent) =
	if is_honest(X)
	then (
		(*get the variables*)
		let var_all_X = cAll(X, ssionid) in
		in (var_all_X, (ic:nat, old_tk:exponent, old_leaf:exponent, old_stg_k:stage_key, (LEAF_1:G, PAR_12:G, LEAF_2:G, ROOT:G, LEAF_3:G)) );
		event Cell(var_all_X,ic, (old_tk, old_leaf, old_stg_k, (LEAF_1, PAR_12, LEAF_2, ROOT, LEAF_3)));
		let NEW_LEAF = exp(g, new_leaf) in
		if (exp(g, old_leaf) = LEAF_1) then
			let par_X = i(exp(LEAF_2, new_leaf)) in
			let root = i(exp(LEAF_3, par_X)) in
			let stg_k = KDF(root, old_stg_k) in

			(*broadcast updating mesg.*)
			let updt_path = (NEW_LEAF, exp(g, par_X), exp(g, root)) in
			(*update the private channels*)
			event Cell(var_all_X,ic+1, (root, new_leaf, stg_k, ( NEW_LEAF, exp(g, par_X), LEAF_2, exp(g, root), LEAF_3 )));
			out(var_all_X, (ic+1,root, new_leaf, stg_k, ( NEW_LEAF, exp(g, par_X), LEAF_2, exp(g, root), LEAF_3 )) );
			out(c, senc(s, stg_k));
			out(c, ( updt_path, smac(updt_path, old_stg_k) ))

		else (
			if (exp(g, old_leaf) = LEAF_2) then
				let par_X = i(exp(LEAF_1, new_leaf)) in
				let root = i(exp(LEAF_3, par_X)) in
				let stg_k = KDF(root, old_stg_k) in

				(*broadcast updating mesg.*)
				let updt_path = (NEW_LEAF, exp(g, par_X), exp(g, root)) in
				event Cell(var_all_X,ic+1, (root, new_leaf, stg_k, ( LEAF_1, exp(g, par_X), NEW_LEAF, exp(g, root), LEAF_3 )));
				out(var_all_X, (ic+1,root, new_leaf, stg_k, ( LEAF_1, exp(g, par_X), NEW_LEAF, exp(g, root), LEAF_3 )) );
				out(c, senc(s, stg_k));
				out(c, ( updt_path, smac(updt_path, old_stg_k) ))

			else
				let root = i(exp(PAR_12, new_leaf)) in
				let stg_k = KDF(root, old_stg_k) in

				let updt_path = (NEW_LEAF, exp(g, root)) in
				event Cell(var_all_X,ic+1, (root, new_leaf, stg_k, ( LEAF_1, PAR_12, NEW_LEAF, exp(g, root), NEW_LEAF )));
				out(var_all_X, (ic+1,root, new_leaf, stg_k, ( LEAF_1, PAR_12, NEW_LEAF, exp(g, root), NEW_LEAF )) );
				out(c, senc(s, stg_k));
				out(c, ( updt_path, smac(updt_path, old_stg_k) ))

		)
	).

let memRecvUpdt(ssionid:ssnid, X:id) =
	if is_honest(X)
	then (
		in(c, ( m:bitstring, mac:bitstring )); (*get the updating msg.*)

		(*get values from private channels*)
		let var_all_X = cAll(X, ssionid) in
		in (var_all_X, (ic:nat,old_tk:exponent, leaf_X:exponent, old_stg_k:stage_key, (LEAF_1:G, PAR_12:G, LEAF_2:G, ROOT:G, LEAF_3:G)) );
		event Cell(var_all_X,ic, (old_tk, leaf_X, old_stg_k, (LEAF_1, PAR_12, LEAF_2, ROOT, LEAF_3)));
		if smac(m, old_stg_k) = mac then (*check MAC*)
		( let (NEW_LEAF:G, NEW_INTERM:G, ROOT:G) = m in (*updater is either leaf_1 or leaf_2*)
				( if (exp(g, leaf_X) = LEAF_1) then (*then the updated path is from leaf 2*)
						let new_par = i(exp(NEW_LEAF, leaf_X)) in
						let root = i(exp(LEAF_3, new_par)) in

						let new_stg_k = KDF(root, old_stg_k) in

						event Cell(var_all_X,ic+1, (root, leaf_X, new_stg_k, ( LEAF_1, exp(g, new_par), NEW_LEAF, exp(g, root), LEAF_3 )));
						out(var_all_X, (ic+1,root, leaf_X, new_stg_k, ( LEAF_1, exp(g, new_par), NEW_LEAF, exp(g, root), LEAF_3 )) );
						out(c, senc(s, new_stg_k))

					else
						(
							if (exp(g, leaf_X) = LEAF_2) then (*the updated path is from leaf 1*)
								let new_par = i(exp(NEW_LEAF, leaf_X)) in
								let root = i(exp(LEAF_3, new_par)) in

								let new_stg_k = KDF(root, old_stg_k) in

								event Cell(var_all_X,ic+1, (root, leaf_X, new_stg_k, ( NEW_LEAF, exp(g, new_par), LEAF_2, exp(g, root), LEAF_3 )));
								out(var_all_X, (ic+1,root, leaf_X, new_stg_k, ( NEW_LEAF, exp(g, new_par), LEAF_2, exp(g, root), LEAF_3 )) );
								out(c, senc(s, new_stg_k))

							else (*the receiver is leaf_3, just need the NEW_PAR in th updated path*)
								let root = i(exp(NEW_INTERM, leaf_X)) in
								let new_stg_k = KDF(root, old_stg_k) in
								event Cell(var_all_X,ic+1, (root, leaf_X, new_stg_k, ( LEAF_1, NEW_INTERM, LEAF_2, exp(g, root), LEAF_3 )));
								out(var_all_X, (ic+1,root, leaf_X, new_stg_k, ( LEAF_1, NEW_INTERM, LEAF_2, exp(g, root), LEAF_3 )) );
								out(c, senc(s, new_stg_k))
						)
				)
			else let (NEW_LEAF:G, ROOT:G) = m in (*updater is leaf_3*)
				( if (exp(g, leaf_X) = LEAF_1) then (*the receiver is leaf_1*)
						let root = i(exp( NEW_LEAF, i(exp(LEAF_2, leaf_X)) )) in

						let new_stg_k = KDF(root, old_stg_k) in
						event Cell(var_all_X,ic+1, (root, leaf_X, new_stg_k, ( LEAF_1, PAR_12, LEAF_2, exp(g, root), NEW_LEAF )));
						out(var_all_X, (ic+1,root, leaf_X, new_stg_k, ( LEAF_1, PAR_12, LEAF_2, exp(g, root), NEW_LEAF )) );
						out(c, senc(s, new_stg_k))

					else (*the receriver is leaf_2*)
						let root = i(exp( NEW_LEAF, i(exp(LEAF_1, leaf_X)) )) in
						let new_stg_k = KDF(root, old_stg_k) in
						event Cell(var_all_X,ic+1, (root, leaf_X, new_stg_k, ( LEAF_1, PAR_12, LEAF_2, exp(g, root), NEW_LEAF )));
						out(var_all_X, (ic+1,root, leaf_X, new_stg_k, ( LEAF_1, PAR_12, LEAF_2, exp(g, root), NEW_LEAF )) );
						out(c, senc(s, new_stg_k))
				)
		)
	).

let genHonest() =
	! (
		new seed1:s_id;
		out(c, beHonest(seed1));
		let sk = ik(seed1) in
		out(c, pk(sk));
		insert keys(beHonest(seed1), pk(sk))
	).

not attacker(new skS).
not attacker(new seed1).
not attacker(new seed2).
not attacker(ik(new seed1)).
not s:s_id; attacker(cAll(beHonest(s),new j)).

nounif s:s_id, ic:nat, old_tk:exponent, leaf_X:exponent, old_stg_k:stage_key, LEAF:G, PAR:G, LEAF':G, ROOT:G;
	mess(cAll(beHonest( *s),new j),(ic,old_tk,leaf_X,old_stg_k,(exp(g,leaf_X),PAR,*LEAF,ROOT,*LEAF'))).

nounif x:exponent; attacker(exp(g,x)).

let genDishonest() =
	! (
		new seed2:s_id;
		out(c, beDishonest(seed2));
		let sk = ik(seed2) in
		out(c, sk);
		(*(! new ek_X:skey; insert ekeys(beDishonest(seed2), pk(ek_X));
				out(c, ek_X)) | *)
		insert keys(beDishonest(seed2), pk(sk))
	).

lemma s':s_id, ic:nat, old_tk:exponent, old_stg_k:stage_key, leaf:exponent, tree:bitstring;
	event(Cell(cAll(beHonest(s'),new j), ic, (old_tk,leaf,old_stg_k,tree))) && attacker(old_stg_k) ==> false;
	attacker(s) [induction].

query event(Test).

process
	new skS:skey; out(c, pk(skS));
	let pkS = pk(skS) in
	genHonest() | genDishonest() | server(skS)
	| ! (
			new j[]:ssnid;
			in(c, A:id) [precise]; in(c, B:id) [precise]; in(c, C:id) [precise];
			if is_honest(A) && is_honest(B) && is_honest(C) && A <> B && A <> C && B <> C
			then
			(
				  memInitiate(pkS, j, A, B, C) (*A init. a session between A, B, and C*)
				| memTwoInit(pkS, j, B, A) | memThreeInit(pkS, j, C, A)
        (*
				| ! (new newL:exponent;
					memUpdate(j, A, newL) | memRecvUpdt(j, B) | memRecvUpdt(j, C))
				| ! (new newL:exponent;
					memUpdate(j, B, newL) | memRecvUpdt(j, A) | memRecvUpdt(j, C))
				| !(new newL:exponent;
					memUpdate(j, C, newL) | memRecvUpdt(j, A) | memRecvUpdt(j, B))*)

		 	)
		)
