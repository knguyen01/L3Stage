(*set preciseActions = true.*)
set verboseClauses = short.
(*set verboseRules = true.*)

(*Public-key encryption and signature*)
type pkey.
type skey.
const OK:bitstring [data].

fun pk(skey):pkey.
fun aenc(bitstring, pkey):bitstring.

reduc forall m:bitstring, sk:skey;
	adec(aenc(m, pk(sk)), sk) = m.

fun sign(bitstring, skey):bitstring.

reduc forall m:bitstring, sk:skey;
	getmsg(sign(m, sk)) = m.

reduc forall m:bitstring, sk:skey;
	checksign(sign(m , sk), m, pk(sk)) = OK.

(*Mac and symmetric keys*)
type stage_key.
const sk_0:stage_key. (*starting value for stage key, before init.*)
fun senc(bitstring, stage_key):bitstring.

reduc forall m:bitstring, k:stage_key;
	sdec(senc(m, k), k) = m.

fun smac(bitstring, stage_key):bitstring.

type node_skey.

fun KeyExchange(skey, pkey, skey, pkey):node_skey. (*1-round key exchange function*)

equation forall ia:skey, ib:skey, ea:skey, eb:skey;
	KeyExchange(ia, pk(ib), ea, pk(eb)) = KeyExchange(ib, pk(ia), eb, pk(ea)).

fun H(node_skey, node_skey, node_skey):node_skey. (*a hash function for deriving tree key*)
fun KDF(node_skey, stage_key):stage_key. (*KDF takes as input the tk and previous stage key*)

fun pub_from_leaf(node_skey):pkey.

(*abstraction for the computation of tree key, knowing one of the 3 leaves*)
fun retrieveTreeKey(nat, bitstring, node_skey):node_skey
reduc forall x:node_skey, y:node_skey, z:node_skey;
	retrieveTreeKey(1, (pub_from_leaf(x), pub_from_leaf(y), pub_from_leaf(z)), x) = H(x, y, z)

otherwise forall x:node_skey, y:node_skey, z:node_skey;
	retrieveTreeKey(2, (pub_from_leaf(x), pub_from_leaf(y), pub_from_leaf(z)), y) = H(x, y, z)

otherwise forall x:node_skey, y:node_skey, z:node_skey;
	retrieveTreeKey(3, (pub_from_leaf(x), pub_from_leaf(y), pub_from_leaf(z)), z) = H(x, y, z).


fun replace_in_tuple(nat,bitstring,pkey):bitstring
reduc
  forall pos:nat, x:pkey, y:pkey, z:pkey, x':pkey; replace_in_tuple(1,(x,y,z),x') = (x',y,z)
  otherwise forall pos:nat, x:pkey, y:pkey, z:pkey, y':pkey; replace_in_tuple(2,(x,y,z),y') = (x,y',z)
  otherwise forall pos:nat, x:pkey, y:pkey, z:pkey, z':pkey; replace_in_tuple(3,(x,y,z),z') = (x,y,z').


type id.
type s_id.
type ssnid.

(*Events' definitions*)
event Test.
event Cell(channel,nat,bitstring).
event Reveal(ssnid, nat, id, bitstring). (*'Reveal' event is indexed by the session id, stage id, and the content of the corresponding 'cAll'*)
event HonestUpdt(ssnid, nat, id). (*raised whenever an honest party does an update for his leaf*)


(*Axioms' definitions*)
axiom d:channel, i:nat, s:bitstring, s':bitstring;
	event(Cell(d,i,s)) && event(Cell(d,i,s')) ==> s = s'.

(*for foward secrecy*)
axiom j:ssnid, i:nat, C:id, content:bitstring, j':ssnid, i':nat, C':id, content':bitstring;
  event(Reveal(j, i, C, content)) && event(Reveal(j', i', C', content'))
    ==> j = j' && i = i' && C = C' && content = content'.


(*the following 3 functions will be used in function
'genHonest' and 'genDishonest', in order to create honest and dishonest
members*)
fun beHonest(s_id):id [private].
fun beDishonest(s_id):id [private].
fun ik(s_id):skey [private]. (*generate a fresh identity key for the input seed id*)

fun ik_of_id(id):skey
reduc
	forall x:s_id; ik_of_id(beHonest(x)) = ik(x)
	otherwise forall x:s_id; ik_of_id(beDishonest(x)) = ik(x) [private] .

fun is_dishonest(id):bool
reduc forall s:s_id; is_dishonest(beDishonest(s)) = true.

fun is_honest(id):bool
reduc forall s:s_id; is_honest(beHonest(s)) = true.

table keys(id, pkey). (*held by the server to provide the public identity key*)
table ekeys(id, pkey). (*held by the server to provide the public ephemeral key*)


free c:channel.
fun cAll(id, ssnid):channel [private].
(*free s:bitstring [private].*)

let server(skS:skey) =
	!
  in(c, R:id); (*queries for one-time ephemeral keys and identity keys*)
	get keys(=R, IK_R) in
	get ekeys(=R, EK_R) in
	out( c, (R, sign((R,IK_R, EK_R), skS)) ).

let memInitiate(pkS:pkey, ssionid:ssnid, X:id, Y:id, Z:id) =
	if is_honest(X) then
	( out(c, Y);out(c, Z);
		let ik_X = ik_of_id(X) in
		let var_all_X = cAll(X, ssionid) in

		new ek_X[]:skey; insert ekeys(X, pk(ek_X));
		in(c, (=Y, m_IK_Y:bitstring));
		in(c, (=Z, m_IK_Z:bitstring));

		if checksign(m_IK_Y, getmsg(m_IK_Y), pkS) = OK then
		let (=Y,IK_Y:pkey, EK_Y:pkey) = getmsg(m_IK_Y) in

		if checksign(m_IK_Z, getmsg(m_IK_Z), pkS) = OK then
		(	let (=Z,IK_Z:pkey, EK_Z:pkey) = getmsg(m_IK_Z) in

			new leaf_X[]:node_skey;
			let leaf_Y = KeyExchange(ik_X, IK_Y, ek_X, EK_Y) in
			let leaf_Z = KeyExchange(ik_X, IK_Z, ek_X, EK_Z) in

			let abstr_brdcst_tree = (pub_from_leaf(leaf_X), pub_from_leaf(leaf_Y), pub_from_leaf(leaf_Z)) in
			let lst_EKs = (pk(ek_X), EK_Y, EK_Z) in
			let tk = retrieveTreeKey(1, abstr_brdcst_tree, leaf_X) in
			let stg_k = KDF(tk, sk_0) in
			out(c, sign((lst_EKs, abstr_brdcst_tree), ik_X));
			(*why in this order it works, and putting the 'event' before outputing the tree doesn't ?*)
			event Cell(var_all_X, 0, (1, stg_k, leaf_X, abstr_brdcst_tree));
			out(var_all_X, (0, 1, stg_k, leaf_X, abstr_brdcst_tree)) (*save the internal view on stg key, his own leaf and the tree*)
  	)
	).


let memRecvInit(pkS:pkey, ssionid:ssnid, X:id, I:id) =
	if is_honest(X)
  then
  	(
      let ik_X = ik_of_id(X) in

  		new ek_X[]:skey;
      insert ekeys(X, pk(ek_X));

      out(c, I);
  		in(c, (=I, m_IK_I:bitstring));

  		if checksign(m_IK_I, getmsg(m_IK_I), pkS) = OK then
  		let (=I, IK_I:pkey, EK_I:pkey) = getmsg(m_IK_I) in

  		in(c, msg:bitstring);

  		if checksign(msg, getmsg(msg), IK_I) = OK then
  		let ((EK_I:pkey, EK_2:pkey, EK_3:pkey), (pub_1:pkey, pub_2:pkey, pub_3:pkey)) = getmsg(msg) in

  		let var_all_X = cAll(X, ssionid) in
  		let leaf_X = KeyExchange(ik_X, IK_I, ek_X, EK_I) in
  		if EK_2 = pk(ek_X)
      then
  			(
          let tk = retrieveTreeKey(2, (pub_1, pub_2, pub_3), leaf_X ) in
    			let stg_k = KDF(tk, sk_0) in

    			event Cell(var_all_X, 0, (2, stg_k, leaf_X, (pub_1, pub_2, pub_3)));
    			out(var_all_X, (0, 2, stg_k, leaf_X, (pub_1, pub_2, pub_3)))
  			)
  		else if EK_3 = pk(ek_X)
      then
        (
    			let tk = retrieveTreeKey(3, (pub_1, pub_2, pub_3), leaf_X ) in
    			let stg_k = KDF(tk, sk_0) in

    			event Cell(var_all_X, 0, (3, stg_k, leaf_X, (pub_1, pub_2, pub_3)));
    			out(var_all_X, (0, 3, stg_k, leaf_X, (pub_1, pub_2, pub_3)))
        )
      else 0
  	).

let memUpdate(ssionid:ssnid, X:id, new_leaf:node_skey) =
	if is_honest(X) then
	(
    let var_all_X = cAll(X, ssionid) in
		in (var_all_X, (ic:nat, pos:nat, old_stg_k:stage_key, old_leaf:node_skey, (old_node_1:pkey, old_node_2:pkey, old_node_3:pkey)) );
    (*
    What should be true :
    if pos = i then pub_from_leaf(old_leaf) = old_node_i
    old_stg_k = KDF(H(leaf1,leaf2,leaf3),old_stg_k') with old_node_i = pub_from_leaf(leaf_i)
    *)
		event Cell( var_all_X, ic, (old_stg_k, old_leaf, (old_node_1, old_node_2, old_node_3)) );

    let new_tree = replace_in_tuple(pos,(old_node_1,old_node_2,old_node_3),pub_from_leaf(new_leaf)) in
		let new_tk = retrieveTreeKey(pos, new_tree, new_leaf) in
		let new_stg_k = KDF(new_tk, old_stg_k) in

		event Cell(var_all_X, ic+1, (pos,new_stg_k, new_leaf, new_tree));
		event HonestUpdt(ssionid, ic+1, X);
		out(var_all_X, (ic+1, pos,new_stg_k, new_leaf, new_tree));
		out(c, (new_tree, smac(new_tree, old_stg_k)))
	).

let memRecvUpdt(ssionid:ssnid, X:id) =
	if is_honest(X)
  then
    (
      let var_all_X = cAll(X, ssionid) in
    	in (var_all_X, (ic:nat, pos:nat, old_stg_k:stage_key, old_leaf:node_skey, (old_node_1:pkey, old_node_2:pkey, old_node_3:pkey)) );
    	event Cell( var_all_X, ic, (pos,old_stg_k, old_leaf, (old_node_1, old_node_2, old_node_3) ));

    	in(c, (updt_msg:bitstring, mac_updt_msg:bitstring));

    	if smac(updt_msg, old_stg_k) = mac_updt_msg then
    	let (pub_1:pkey, pub_2:pkey, pub_3:pkey) = updt_msg in

      let new_tk = retrieveTreeKey(pos, (pub_1, pub_2, pub_3), old_leaf ) in
      let new_stg_k = KDF(new_tk, old_stg_k) in
      event Cell( var_all_X, ic+1, (pos,new_stg_k, old_leaf, (pub_1, pub_2, pub_3)) );
      out(var_all_X, (ic+1, pos,new_stg_k, old_leaf, (pub_1, pub_2, pub_3)))
    ).

let genHonest() =
	! (
		new seed1:s_id;
		out(c, beHonest(seed1));
		let sk = ik(seed1) in
		out(c, pk(sk));
		insert keys(beHonest(seed1), pk(sk))
	).

let genDishonest() =
	! (
		new seed2:s_id;
		out(c, beDishonest(seed2));
		let sk = ik(seed2) in
		out(c, sk);
		(*(! new ek_X:skey; insert ekeys(beDishonest(seed2), pk(ek_X));
				out(c, ek_X)) | *)
		insert keys(beDishonest(seed2), pk(sk))
	).

not attacker(new skS).
not attacker(new seed1).
not attacker(new seed2).

(*not attacker(ik(new seed1)).*)
not s:s_id; attacker(cAll(beHonest(s),new j)).

(*1 pub_from_leaf and 2 pub_node*)
nounif s:s_id, ic:nat, pos:nat, old_stg_k:stage_key, leaf:node_skey, content:bitstring;
	mess(cAll(beHonest( *s),new j),(ic, *pos, *old_stg_k, *leaf, *content)).

(*
nounif s:s_id, ic:nat, pos:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_2:pkey, old_node_3:pkey;
	mess(cAll(beHonest( *s),new j),(ic, *pos, *old_stg_k, *leaf,( pub_from_leaf( *old_node_key),*old_node_2,*old_node_3))).

nounif s:s_id, ic:nat, pos:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_1:pkey, old_node_2:pkey, old_node_3:pkey;
	mess(cAll(beHonest( *s),new j),(ic, *pos, *old_stg_k, *leaf,( *old_node_1,pub_from_leaf( *old_node_key),*old_node_3))).

nounif s:s_id, ic:nat, pos:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_1:pkey, old_node_2:pkey, old_node_3:pkey;
	mess(cAll(beHonest( *s),new j),(ic, *pos, *old_stg_k, *leaf,( *old_node_1,*old_node_2,pub_from_leaf( *old_node_key)))).
*)

(*2 pub_from_leaf and 1 pub_node*)
(*nounif s:s_id, ic:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_key1:node_skey, old_node_2:pkey;
	mess(cAll(beHonest( *s),new j),(ic, old_stg_k, leaf,( pub_from_leaf( *old_node_key1),old_node_2,pub_from_leaf( *old_node_key)))).

nounif s:s_id, ic:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_key1:node_skey, old_node_2:pkey;
	mess(cAll(beHonest( *s),new j),(ic, old_stg_k, leaf,( pub_from_leaf( *old_node_key1), pub_from_leaf( *old_node_key), old_node_2))).

nounif s:s_id, ic:nat, old_stg_k:stage_key, leaf:node_skey, old_node_key:node_skey, old_node_key1:node_skey, old_node_2:pkey;
	mess(cAll(beHonest( *s),new j),(ic, old_stg_k, leaf,( old_node_2, pub_from_leaf( *old_node_key1), pub_from_leaf( *old_node_key)))).*)

(*from nounif generated*)
(*nounif s:s_id, s_1:s_id, ic:nat, old_stg_k:stage_key, leaf:node_skey,
			ek_X:skey, pk_ek_X_2:pkey,
			node_pub_1:pkey, node_pub_2:pkey, node_pub_3:pkey;
	mess(cAll(beHonest( *s),new j),(ic,old_stg_k,KeyExchange(ik( *s_1),pk(ik( *s)),*ek_X,*pk_ek_X_2),(node_pub_1,node_pub_2,node_pub_3))).*)

(*foward secrecy*)
event IDS(ssnid,id,id,id).
lemma s:s_id, s':s_id, js:ssnid, ic:nat, ic':nat, stg_k:stage_key, pos:nat,
			stg_k':stage_key, leaf:node_skey, tree:bitstring, leaf':node_skey, tree':bitstring, leaf1:node_skey,leaf2:node_skey,leaf3:node_skey,
			pub_1:pkey, pub_2:pkey, pub_3:pkey,
			IK_I:pkey, ek_X:skey, EK_I:pkey;

 	event( Cell(cAll(beHonest(s), js), ic, (pos,stg_k, leaf, tree )) ) && attacker(stg_k)
 		==> event( Reveal( js, ic', beHonest(s'), (stg_k', leaf', tree' )) ) && (ic >= ic');

	event(Cell(cAll(beHonest(s),js), ic, (1,stg_k, leaf, (pub_1,pub_2,pub_3)))) ==> pub_from_leaf(leaf) = pub_1;
	event(Cell(cAll(beHonest(s),js), ic, (2,stg_k, leaf, (pub_1,pub_2,pub_3)))) ==> pub_from_leaf(leaf) = pub_2;
	event(Cell(cAll(beHonest(s),js), ic, (3,stg_k, leaf, (pub_1,pub_2,pub_3)))) ==> pub_from_leaf(leaf) = pub_3;

	event(Cell(cAll(beHonest(s),js), ic, (1,stg_k, leaf, (pub_1,pub_2,pub_3)))) && attacker(leaf);
	event(Cell(cAll(beHonest(s),js), ic, (2,stg_k, leaf, (pub_1,pub_2,pub_3)))) && attacker(leaf);
	event(Cell(cAll(beHonest(s),js), ic, (3,stg_k, leaf, (pub_1,pub_2,pub_3)))) && attacker(leaf) ==>
		event( Reveal( js, ic', beHonest(s'), (stg_k', leaf', tree' )) ) && (ic >= ic')

	(*
	event(Cell(cAll(beHonest(s),js), ic, (pos,stg_k, leaf, (pub_1,pub_2,pub_3)))) ==>
		stg_k = KDF(H(leaf1,leaf2,leaf3),stg_k') &&
		pub_from_leaf(leaf1) = pub_1 &&
		pub_from_leaf(leaf2) = pub_2 &&
		pub_from_leaf(leaf3) = pub_3;


	event(Reveal(Reveal( js, ic', beHonest(s'), (stg_k', leaf', tree' )))) && attacker(ik(s)) ==> s = s'*)

	[fullSat,induction].

query attacker(c).

(*post-compromise secrecy - TBC*)

(*query attacker(s).
query event(Test).*)

process
	new skS:skey; out(c, pk(skS));
	let pkS = pk(skS) in
	genHonest() | genDishonest() | server(skS)
	| (
			new j[]:ssnid;
			in(c, A:id) [precise]; in(c, B:id) [precise]; in(c, C:id) [precise];
			event IDS(j,A,B,C);
			if is_honest(A) && is_honest(B) && is_honest(C) && A <> B && A <> C && B <> C
			then
			(
				  memInitiate(pkS, j, A, B, C) (*A init. a session between A, B, and C*)
				| memRecvInit(pkS, j, B, A) | memRecvInit(pkS, j, C, A)
				| (!new newL[]:node_skey;
					memUpdate(j, A, newL) | memRecvUpdt(j, B) | memRecvUpdt(j, C))
				(*| ! (new newL:node_skey;
					memUpdate_2nd(j, B, newL) | memRecvUpdt(j, A) | memRecvUpdt(j, C))
				| ! (new newL:node_skey;
					memUpdate_3rd(j, C, newL) | memRecvUpdt(j, A) | memRecvUpdt(j, B))*)
				| (
						in(c, COM:id);
						if (COM = C) then
						( let var_all = cAll(COM, j) in
							in (var_all, (ic:nat, pos:nat,stg_k:stage_key, leaf:node_skey, (pub_1:pkey, pub_2:pkey, pub_3:pkey)));
							event Cell( var_all, ic, (pos,stg_k, leaf, (pub_1, pub_2, pub_3)) );

							event Reveal(j, ic, COM, (stg_k, leaf, (pub_1, pub_2, pub_3)));
							out(c, (ik_of_id(COM), j, COM, stg_k, leaf, (pub_1, pub_2, pub_3)));

							out(var_all, (ic, pos, stg_k, leaf, (pub_1, pub_2, pub_3)))
						)
					)
		 	)
		)
