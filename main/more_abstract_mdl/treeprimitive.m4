dnl // Tamarin uses ' which is an m4 close quote. So use <! !> for quoting instead.
changequote(<!,!>)
changecom(<!@,@!>)

theory treeprimitive
begin

builtins: multiset, diffie-hellman, signing
functions: kdf/1 // 'kdf' accepts 1 argument
functions: mac/2 // 'mac' accepts 2 arguments

/*
~x denotes x:fresh
#x denotes x:temporal
$x denotes x:pub
m denotes m:message
*/

restriction UniqueTime: "All x #i #j. Unique(x) @ i & Unique(x) @ j ==> #i = #j"
restriction Equality_checks: "All x y #i. Eq(x,y) @ i ==> x = y"

define(GroupKey, F_GroupKey($*))
define(GroupIKs, F_GroupIKs($*))
define(Ltk, F_Ltk($*))
define(Pk, F_Pk($*))
define(EPk, F_EPk($*))
define(GroupBuildInvariants, F_GroupBuildInvariants($*))

/* Generate long-term keypair */
/* When using the Tamarin default set-up,
there is only one public channel modeling the network controlled by
the adversary
*/
rule generate_ltk:
   let pkA = 'g'^~ikA 
   in
   [ Fr(~ikA) ]  // generate a fresh name ik, which is a new private key
   --[ RegKey($A) ]-> // the name $A of the agent is chosen non-deterministic
   [ !Ltk( $A, ~ikA ) // generate the fact, ! indicates that the fact is persistent, 
   // this fact associates A with its long-term private key ikA
   , !Pk( $A, pkA, pk(~ikA) ) // generate the fact
   , Out( pkA ) // output pkA to the public channel
   ]

/* pre-registration */
/*
From a freshly generated prekey, if an agent $X needs,
then that prekey EK is registered for $X and it is signed with $X's long-term
identity key;
note that the association between $X and the prekey is non-persistent;
hence, it models the one-time usage of the prekey
*/
rule Register_prekey:
    let EK = 'g'^~ek
    in
    [ Fr( ~ek )
    , !Ltk( $X, ~ik ) 
    ]
    --[ RegisteredPrekey('g'^~ik, EK) ]->
    [ PreKey( ~ek, $X, ~ik )
    , !EPk('g'^~ik,EK)
    , Out( <EK, sign{EK}~ik> )
    ]

//Group Building
/*
This rule captures the creation of a group, from an initiator Alice;
starting from Alice's leaf key, her chosen set-up key, and 2 auxiliary param.'s,
the facts GroupBuild() and GroupBuildInvariant() - capturing the information regarding the group -
are added to the system state;
note that Alice's public long-term identity key and public chosen set-up key
will be published so as to enable others to agree on the leaf keys in DH tree
*/
rule New_group:
    let KS = 'g'^~ks
        IKA = 'g'^~ikA 
    in
    [ !Ltk($A, ~ikA)
    , Fr(~gid) //Group identifier
    , Fr(~lkA) //Alice's leaf key
    , Fr(~ks)  //Group setup key
    , Fr(~t) //For use with the injectivity lemma
    ]
  --[ LeafKey(~gid, 'g'^~ikA, 'g'^~ikA, ~lkA, pk(~lkA))
    , LeafKeySet(~gid, 'LeafPublics' + <'k',IKA,pk(~lkA)>)
    , Injectivity(~gid, 'NewGroup', ~t)
    ]->
    [ GroupBuild(~gid, ~t, ~ikA, ~ks, ~lkA,
                 'LeafKeys' + <'k',IKA,~lkA>, 
                 'LeafPublics' + <'k',IKA,pk(~lkA)> ) // Q: what is the semantics here? Adding a tuple to a str?
    , !GroupBuildInvariants(~ikA, ~ks, ~lkA, ~gid)
    , Out(<pk(~lkA), 'g'^~ks>) //Public leaf key, and public setup key
    ]

/*
Roughly speaking, the rule 'New_group' captures the very first step to initialize a new group,
with initator Alice. The new group contains only Alice herself, and her public IKA as well as g^ks(SUK)
are published on the common channel.

Next, the rule 'Add_leaf' models the steps where Alice do the one-round AKE(asynchronously) with
each member, and record the corresponding leafkey, so as to build the tree later.
*/

rule Add_leaf:
    let IKL = 'g'^~ikl
        EKL = 'g'^ekl 
        //^ The above are equivalent to group element checks, as long as we do not use the terms in the exponents
        IKA = 'g'^~ikA
        KS  = 'g'^~ks
        lk  = kdf(<EKL^~ikA, IKL^~ks, EKL^~ks, IKL^~ikA>) // this models the one-round AKE
    in
    [ !Ltk($A, ~ikA)
    , !Pk($L, IKL, IKLsigning) //Authenticated distribution of leaf identity
    , GroupBuild(~gid, ~t, ~ikA, ~ks, ~lkA, leafkeys, leafpublics)
    // the fact for group creation is not necessarily persistent because an initiator can 
    // re-init a new group for a new session at any time.
    , !GroupBuildInvariants(~ikA, ~ks, ~lkA, ~gid)
    , Fr(~t2) //For injectivity lemma
    , In(<EKL, EphSig>) //Ephemeral signed under a long-term identity key
    //c.f. rule 'Register_prekey'
    ]
  --[ LeafKey(~gid, IKA, IKL, lk, pk(lk))
    , LeafKeySet(~gid, leafpublics + <'k',IKL,pk(lk)>)
    , Eq(true, verify(EphSig, EKL, IKLsigning)) 
    // we need to verify the signature coming with the prekey
    // cf. rule 'Register_prekey', whenever we assoc. a new prekey to an agent,
    // we sign the prekey with his/her private long-term id. key 
    , UsingPrekey(IKL, EKL) // associate the prekey EKL with this current leaf --> 
    // wont be available to use this prekey for another leaf, cf. lemma 'registered_prekey'
    , Injectivity(~gid, ~t, ~t2)
    ]->
    [ // augment the fact 'GroupBuild', which results from the rule 'New_group' above
    GroupBuild(~gid, ~t2, ~ikA, ~ks, ~lkA, 
      leafkeys + <'k',IKL,lk>, 
      leafpublics + <'k',IKL,pk(lk)>)
    , Out(pk(lk))
    ]


/* Generating the group at Alice, starting with her as the first leaf */
/* This is the true group creation:
- it 'consumes' a non-persistent fact GroupBuild, a persistent fact GroupBuildInvariant and 'leafpublics'
which is updated through rule 'Add_leaf';
- 2 new persistent facts are added to the sys. state: 'Group' containing the leafkey of initiator Alice,
group id and all public leaf keys; 'GroupKeys' containing the group id, the private set-up key,
the private leaf keys and the public leaf keys
 */
rule Finalize:
    [ GroupBuild(~gid, ~t, ~ikA, ~ks, ~lkA, leafkeys, leafpublics)
    , !GroupBuildInvariants(~ikA, ~ks, ~lkA, ~gid)
    , In(leafpublics) //This is for deconstruction chains in tamarin. Since these are all public values,
                      //we can take them from the network to guarantee the adversary knows them
                      //without writing our own 'sources' lemma
    ]
  --[ LeafKeySet(~gid, leafpublics)
    ]->
    [ !Group(~gid, ~lkA, leafpublics) 
    , !GroupKeys(~gid, ~ks, leafkeys, leafpublics)
    ]

/*
Maybe it just models the action of publishing the pubkey DH tree
of the initiator, at the end of the creation
*/

rule EncryptList:
    let leafkeys = <'k',IKL,lk> + otherkeys
        leafpublics  = <'k',IKL,pk(lk)> + otherpublics
    in
    [ !GroupKeys(~gid, ~ks, leafkeys, leafpublics)
    // we can think of this In() function, using pattern-matching, chcking the type
    // of 'leafpublics'
    , In(leafpublics) //This is for deconstruction chains in tamarin. Since these are all public values
                      //we can take them in from the network to guarantee the adversary knows them
                      //without writing our own 'sources' lemma
    ]
  --[ SentLeaves(leafpublics, lk)
    ]->
    [ Out(< leafpublics, mac(<'g'^~ks, leafpublics>,lk) >)
    ]


/* Receiving the 'added to group' message */
/*
The 'kdf' function is defined as in the authenticated version of protocol, described in the paper;
more specifically, $A(ltk = g^ikA = g^a, ek = g^ekA = g^x) and $L(ltk = g^ikL = g^b, ek = g^ekL = g^y) 
will agree on 
K = kdf(<g^ab, g^xy, g^ay, g^bx>) 
  = kdf(<IKA^ikL, EKA^ekL, IKA^ekL, EKA^ikL> (on the side of L)
  = kdf(<IKL^ikA, EKL^ekA, IKL^ekA, EKL^ikA> (on the side of A)

*/
rule LeafMember:
    let IKA = 'g'^~ika //there are equivalent to group element checks, we don't use the terms in the exponent
        EKA = 'g'^eka // Q: is ekA not fresh?
        lk  = kdf(<IKA^~ekL, EKA^~ikL, EKA^~ekL, IKA^~ikL>)
        leafpublics = <'k','g'^~ikL,pk(lk)> + <'k',IKA,LKA> + otherlks
    in
    [ // The agent's registered prekey and identiy
      PreKey(~ekL, $L, ~ikL)
    , !Ltk($L, ~ikL)
      // Authenticated distribution of Alice's identity
    , !Pk($A, IKA, IKAsigning)
      // The agent's generated group identifier
    , Fr(~gid)
      // The public setup key from Alice, leaf public keys, and a MAC under the leaf key
    , In(< EKA, leafpublics, mac(<EKA, leafpublics>,lk) >)
    ]
  --[
      GotLeafKey(IKA, 'g'^~ikL, lk, pk(lk))
    , GotLeaves(leafpublics)
    ]->
    [ !Group(~gid, lk, leafpublics)
    ]

/* Key Reveal */
rule IK_reveal:
   [ !Ltk($A, ~kA) ] --[ Reveal('g'^~kA) ]-> [ Out(~kA) ]

// This acts a group key oracle: it associates a unique group key to each multiset of public terms
/*
They abstract out the tree and the process of derivation the root key from leaves and 
intermediate nodes;
instead, this rule acts as an 'oracle' and assigns to each group 'gid' a group key;
the reason why they can abstract this way, as shown in the security proof, is that the root key
is indistinguishable from a randomly generated DH key;

The condition to query this 'oracle' is expressed in 'In(lkeys)', 
that is, one must know one leaf key
*/
rule ComputeGroupKey:
    [ In(lkeys)
    , Fr(~groupkey)
    ]
  --[ Unique(lkeys) // corr. to the chall.'s behavior in Game 1 in the security proof 
    ]->
    [ !GroupKey(~groupkey,lkeys)
    , !Revealable(~groupkey) // This persistent fact, again, is not used anywhere
    ]

// Group key oracle for the adversary, where secret can be provided over the network
rule AdvKeyOracle:
    [ In(lkey) // model the corruption of a party, i.e. the adversary must know at least 1 leaf key
    , !GroupKey(~gkey, otherkeys + <'k',IKL,pk(lkey)>)
    ]
  --[ LearnedKey(~gkey, <'k',IKL,pk(lkey)>)
    ]->
    [ Out(~gkey)
    ]

// Group key oracle for the adversary, where secret is provided in an authenticated way
rule AgentKeyOracle:
    let leafpublics = <'k',IKL,pk(lk)> + otherleaves
    in
    [ !Group(~gid, lk, leafpublics)
    , !GroupKey(~gkey, leafpublics)
    ]
  --[ Accept(~gid, leafpublics, ~gkey)
    , AcceptLeafKey(lk)
    ]->
    [ ]

/*
///////// Exists-trace lemmas, one for a three-party group, and one
///////// to ensure that it is possible for the adversary to learn
///////// the group key
*/
define(boundedgroup, <!'LeafPublics'+<'k',ikl1,pk(k1)>+<'k',ikl2,k2>+<'k',ikl3,k3>!>)

lemma bounded_trace: exists-trace
    "Ex #i gid ikl1 ikl2 ikl3 k1 k2 k3 gk.
        Accept(gid, boundedgroup, gk) @ i
        & not(Ex #k key. Reveal(key) @ k)"
simplify
solve( Accept( gid,
               ('LeafPublics'+<'k', ikl1, pk(k1)>+
                <'k', ikl2, k2>+<'k', ikl3, k3>),
               gk
       ) @ #i )
  case AgentKeyOracle_case_1
  solve( !F_GroupKey( ~gkey,
                      ('LeafPublics'+<'k', ikl1, pk(k1)>+
                       <'k', ikl2, k2>+<'k', ikl3, k3>)
         ) ▶₁ #i )
    case ComputeGroupKey
    solve( !Group( ~gid, k1,
                   ('LeafPublics'+<'k', ikl1, pk(k1)>+
                    <'k', ikl2, k2>+<'k', ikl3, k3>)
           ) ▶₀ #i )
      case Finalize_case_1
      solve( GroupBuild( ~gid, ~t.1, ~ikA, ~ks, ~lkA, leafkeys,
                         ('LeafPublics'+<'k', ikl1, pk(~lkA)>+
                          <'k', ikl2, k2>)
             ) ▶₂ #vr.2 )
        case Add_leaf
        solve( GroupBuild( ~gid, ~t.3, ~ikA, ~ks, ~lkA, leafkeys,
                           ('LeafPublics'+<'k', ikl1, pk(~lkA)>)
               ) ▶₂ #vr.6 )
          case New_group
          solve( !KU( sign(z.4, ~x) ) @ #vk.18 )
            case Register_prekey
            solve( !KU( sign(z.2, ~x.1) ) @ #vk.21 )
              case Register_prekey
              solve( !KU( pk(~lkA) ) @ #vk.12 )
                case New_group
                solve( !KU( 'g'^~x ) @ #vk.18 )
                  case generate_ltk
                  solve( !KU( pk(kdf(<'g'^(~ek*~ikA), 
                                      'g'^(~ks*~x), 'g'^(~ek*~ks), 
                                      'g'^(~ikA*~x)>))
                         ) @ #vk.19 )
                    case Add_leaf
                    solve( GroupBuild( ~gid, ~t.3, ~ikA, ~ks, ~lkA, leafkeys,
                                       leafpublics
                           ) ▶₂ #vr.13 )
                      case Add_leaf
                      solve( !KU( sign(z.2, ~x.2) ) @ #vk.24 )
                        case Register_prekey
                        solve( GroupBuild( ~gid, ~t.4, ~ikA, ~ks, ~lkA, leafkeys,
                                           leafpublics
                               ) ▶₂ #vr.14 )
                          case New_group
                          solve( !KU( 'g'^~x.1 ) @ #vk.18 )
                            case generate_ltk
                            solve( !KU( pk(kdf(<'g'^(~ikA*~ek.1), 
                                                'g'^(~ks*~x.1), 'g'^(~ks*~ek.1), 
                                                'g'^(~ikA*~x.1)>))
                                   ) @ #vk.19 )
                              case Add_leaf
                              solve( GroupBuild( ~gid, ~t.3, ~ikA, ~ks, ~lkA, leafkeys,
                                                 leafpublics
                                     ) ▶₂ #vr.13 )
                                case Add_leaf
                                solve( !KU( sign(z.2, ~x.2) ) @ #vk.24 )
                                  case Register_prekey
                                  solve( GroupBuild( ~gid, ~t.4, ~ikA, ~ks, ~lkA, leafkeys,
                                                     leafpublics
                                         ) ▶₂ #vr.14 )
                                    case New_group
                                    solve( !KU( 'g'^~ikA ) @ #vk.13 )
                                      case generate_ltk
                                      solve( !KU( 'g'^~ek ) @ #vk.14 )
                                        case Register_prekey
                                        SOLVED // trace found
                                      qed
                                    qed
                                  qed
                                qed
                              qed
                            qed
                          qed
                        qed
                      qed
                    qed
                  qed
                qed
              qed
            qed
          qed
        qed
      qed
    next
      case Finalize_case_2
      by sorry
    next
      case LeafMember_case_1
      by sorry
    next
      case LeafMember_case_2
      by sorry
    next
      case LeafMember_case_3
      by sorry
    next
      case LeafMember_case_4
      by sorry
    next
      case LeafMember_case_5
      by sorry
    next
      case LeafMember_case_6
      by sorry
    qed
  qed
next
  case AgentKeyOracle_case_2
  by sorry
next
  case AgentKeyOracle_case_3
  by sorry
qed

lemma sanity_check: exists-trace
    "Ex #i1 #i2 gid leafkeys gk.
        Accept( gid, leafkeys, gk ) @ #i1 & K( gk ) @ #i2"
simplify
solve( !F_GroupKey( ~gkey,
                    (otherleaves+<'k', IKL, pk(lk)>)
       ) ▶₁ #i1 )
  case ComputeGroupKey
  solve( !KU( ~gkey ) @ #vk )
    case AdvKeyOracle
    solve( !Group( ~gid, lk,
                   (otherleaves+<'k', IKL, pk(lk)>)
           ) ▶₀ #i1 )
      case Finalize_case_1
      by sorry
    next
      case Finalize_case_2
      by sorry
    next
      case Finalize_case_3
      solve( GroupBuild( ~gid, ~t.1, ~ikA, ~ks, ~lkA, leafkeys,
                         (x+<'k', IKL, pk(~lkA)>)
             ) ▶₂ #vr.3 )
        case Add_leaf_case_1
        by sorry
      next
        case Add_leaf_case_2
        by sorry
      next
        case New_group
        solve( splitEqs(1) )
          case split_case_2
          solve( !KU( kdf(<z, 'g'^(~ks*~x), z.1, 
                           'g'^(~ikA*~x)>)
                 ) @ #vk.10 )
            case c_kdf
            solve( !KU( sign(z.2, ~x) ) @ #vk.16 )
              case c_sign
              solve( !KU( ~x ) @ #vk.25 )
                case IK_reveal
                solve( !KU( 'g'^(~ks*~x) ) @ #vk.22 )
                  case New_group
                  solve( !KU( 'g'^(~ikA*~x) ) @ #vk.25 )
                    case c_exp
                    solve( !KU( ~ikA ) @ #vk.28 )
                      case IK_reveal
                      solve( splitEqs(3) )
                        case split_case_01
                        solve( !KU( 'g'^(~ks*inv(~ikA)) ) @ #vk.27 )
                          case New_group
                          solve( !KU( 'g'^inv(~ikA) ) @ #vk.26 )
                            case c_exp
                            solve( !KU( pk(~lkA) ) @ #vk.23 )
                              case New_group
                              solve( !KU( pk(kdf(<'g', 'g'^(~ks*~x), 
                                                  'g'^(~ks*inv(~ikA)), 'g'^(~ikA*~x)
                                                 >))
                                     ) @ #vk.26 )
                                case Add_leaf
                                solve( GroupBuild( ~gid, ~t.2, ~ikA, ~ks, ~lkA, leafkeys,
                                                   leafpublics
                                       ) ▶₂ #vr.13 )
                                  case New_group
                                  solve( !KU( 'g'^~x ) @ #vk.26 )
                                    case c_exp
                                    solve( !KU( 'g'^~ikA ) @ #vk.26 )
                                      case c_exp
                                      SOLVED // trace found
                                    qed
                                  qed
                                qed
                              qed
                            qed
                          qed
                        qed
                      qed
                    qed
                  qed
                qed
              qed
            qed
          qed
        qed
      qed
    next
      case LeafMember_case_1
      by sorry
    next
      case LeafMember_case_2
      by sorry
    next
      case LeafMember_case_3
      by sorry
    next
      case LeafMember_case_4
      by sorry
    qed
  qed
qed


/*******************************
///////// Helper lemmas
*******************************/
lemma injectivity[reuse]:
    "All id n1 n2 n3 #i #j on1 on2 #k.
        //Injective facts that are linked by n2
        Injectivity(id,n1,n2) @ i & Injectivity(id,n2,n3) @ j
        //And some other injective fact with the same id with relation to j
        & Injectivity(id,on1,on2) @ k & #k < #j
    ==> //can't be in between (might be equal to #i, though)
        not(#i < #k)"

lemma registered_prekeys[reuse]:
    /*
      If a ephemeral key is used and bound with id. IK at instance i, 
      then therre exists an instance j before i where either the same 
      id has used this ephemeral key or this ephemeral key has been 
      revealed
    */
    "All #i IK EK.
        UsingPrekey(IK,EK) @ i
    ==> (Ex #j. #j < #i & RegisteredPrekey(IK, EK) @ j) | (Ex #j. #j < #i & Reveal(IK) @ j)"

lemma leaf_key_secrecy [reuse]:
    /*
      If a leaf is added at some stage, and its private leafkey is compromised 
      at some other stage, then either the ephemral key is compromised or 
      the key of the initiator is compromised at some instance.
    */
    "All gid pk1 pk2 lk plk #i #j.
        (LeafKey(gid, pk1, pk2, lk, plk ) @ #i & K( lk ) @ #j) 
    ==> (Ex #k. Reveal( pk1 ) @ #k) | (Ex #k. Reveal( pk2 ) @ #k)"


lemma hidden_gotleaves [reuse]:
    /*
      if at some instance i w.r.t a leaf (lk, PLK), and the DH tree contains leafkeys 'grouplks' 
      then either this leafkeys set must be associated to the group in an instane j before i
      or the long-term identity key of the initiator has been leaked(and the adversary forged 
      the group himself)
      or the long-term id. key of the leaf being considered is leaked(and the adversary update 
      the tree himself)
    */
    "All grouplks IKA IKL lk PLK #i.
        (GotLeaves( grouplks ) @ #i &
        (GotLeafKey( IKA, IKL, lk, PLK ) @ #i))
    ==> (Ex gid #j. (#j < #i) & LeafKeySet(gid, grouplks) @ #j)
        | (Ex #j. (#j < #i) & Reveal( IKA ) @ #j)
        | (Ex #j. (#j < #i) & Reveal( IKL ) @ #j)"
simplify
solve( !KU( mac(<z, 
                 (otherlks+
                  <'k', 'g'^~ikL, 
                   pk(kdf(<'g'^(~ekL*~x), z.1, z.2, 'g'^(~ikL*~x)
                          >))
                  >+
                  <'k', 'g'^~x, LKA>)
                >,
                kdf(<'g'^(~ekL*~x), z.1, z.2, 'g'^(~ikL*~x)
                    >))
       ) @ #vk.14 )
  case EncryptList_case_01
  by contradiction /* from formulas */
next
  case EncryptList_case_02
  by contradiction /* from formulas */
next
  case EncryptList_case_03
  by contradiction /* from formulas */
next
  case EncryptList_case_04
  by contradiction /* from formulas */
next
  case EncryptList_case_05
  by contradiction /* from formulas */
next
  case EncryptList_case_06
  by contradiction /* from formulas */
next
  case EncryptList_case_07
  by contradiction /* from formulas */
next
  case EncryptList_case_08
  by contradiction /* from formulas */
next
  case EncryptList_case_09
  by contradiction /* from formulas */
next
  case EncryptList_case_10
  by contradiction /* from formulas */
next
  case EncryptList_case_11
  by contradiction /* from formulas */
next
  case EncryptList_case_12
  by contradiction /* from formulas */
next
  case EncryptList_case_13
  by contradiction /* from formulas */
next
  case EncryptList_case_14
  by contradiction /* from formulas */
next
  case EncryptList_case_15
  by contradiction /* from formulas */
next
  case EncryptList_case_16
  by contradiction /* from formulas */
next
  case EncryptList_case_17
  by contradiction /* from formulas */
next
  case EncryptList_case_18
  by contradiction /* from formulas */
next
  case EncryptList_case_19
  by contradiction /* from formulas */
next
  case EncryptList_case_20
  by contradiction /* from formulas */
next
  case EncryptList_case_21
  by contradiction /* from formulas */
next
  case EncryptList_case_22
  by contradiction /* from formulas */
next
  case EncryptList_case_23
  by contradiction /* from formulas */
next
  case EncryptList_case_24
  by contradiction /* from formulas */
next
  case EncryptList_case_25
  by contradiction /* from formulas */
next
  case EncryptList_case_26
  by contradiction /* from formulas */
next
  case EncryptList_case_27
  by contradiction /* from formulas */
next
  case EncryptList_case_28
  by contradiction /* from formulas */
next
  case EncryptList_case_29
  by contradiction /* from formulas */
next
  case EncryptList_case_30
  by contradiction /* from formulas */
next
  case EncryptList_case_31
  by contradiction /* from formulas */
next
  case EncryptList_case_32
  by contradiction /* from formulas */
next
  case EncryptList_case_33
  by contradiction /* from formulas */
next
  case EncryptList_case_34
  by contradiction /* from formulas */
next
  case EncryptList_case_35
  by contradiction /* from formulas */
next
  case EncryptList_case_36
  by contradiction /* from formulas */
next
  case EncryptList_case_37
  by contradiction /* from formulas */
next
  case EncryptList_case_38
  by contradiction /* from formulas */
next
  case EncryptList_case_39
  by contradiction /* from formulas */
next
  case EncryptList_case_40
  by contradiction /* from formulas */
next
  case EncryptList_case_41
  by contradiction /* from formulas */
next
  case EncryptList_case_42
  by contradiction /* from formulas */
next
  case EncryptList_case_43
  by contradiction /* from formulas */
next
  case EncryptList_case_44
  by contradiction /* from formulas */
next
  case EncryptList_case_45
  by contradiction /* from formulas */
next
  case EncryptList_case_46
  by contradiction /* from formulas */
next
  case EncryptList_case_47
  by contradiction /* from formulas */
next
  case EncryptList_case_48
  by contradiction /* from formulas */
next
  case EncryptList_case_49
  by contradiction /* from formulas */
next
  case EncryptList_case_50
  by contradiction /* from formulas */
next
  case EncryptList_case_51
  by contradiction /* from formulas */
next
  case c_mac
  solve( !KU( kdf(<'g'^(~ekL*~x), z.1, z.2, 
                   'g'^(~ikL*~x)>)
         ) @ #vk.16 )
    case c_kdf
    solve( !F_Ltk( $L, ~ikL ) ▶₁ #i )
      case generate_ltk
      solve( !F_Pk( $A, 'g'^~x, IKAsigning ) ▶₂ #i )
        case generate_ltk
        solve( PreKey( ~ekL, $L, ~ikL ) ▶₀ #i )
          case Register_prekey
          solve( !KU( 'g'^(~ekL*~x) ) @ #vk.18 )
            case Register_prekey
            solve( !KU( ~x ) @ #vk.24 )
              case IK_reveal
              by contradiction /* from formulas */
            qed
          next
            case c_exp
            by solve( !KU( ~ekL ) @ #vk.26 )
          next
            case generate_ltk
            by solve( !KU( ~ekL ) @ #vk.24 )
          qed
        qed
      qed
    qed
  qed
qed


lemma hidden_leafkeysetparts[reuse, use_induction, hide_lemma=registered_prekeys]:
    "All gid ikl lk leafpublics #i.
        LeafKeySet(gid, <'k', ikl, pk(lk)> + leafpublics ) @ i
    ==> (Ex #k pk1. LeafKey(gid, pk1, ikl, lk, pk(lk) ) @ #k)"

lemma leafkeyset_parts[reuse, use_induction, hide_lemma=registered_prekeys]:
    "All gid ikl lk leafpublics gk #i #j.
        LeafKeySet(gid, <'k', ikl, pk(lk)> + leafpublics ) @ i & LearnedKey(gk,<'k',ikl,pk(lk)>) @ j
    ==> (Ex #k pk1. LeafKey(gid, pk1, ikl, lk, pk(lk) ) @ #k)"

lemma gotleaves_parts [reuse]:
    "All grouplks gk ikl lk IKA someagent someleaf #i #j.
        GotLeaves( <'k', ikl, pk(lk)> + grouplks ) @ #i
        & GotLeafKey( IKA, someagent, someleaf, pk(someleaf) ) @ #i
        & LearnedKey( gk, <'k',ikl,pk(lk)> ) @ j
    ==> (Ex gid #j. LeafKey(gid, IKA, ikl, lk, pk(lk)) @ #j)
        | (Ex #j. (#j < #i) & Reveal( IKA ) @ #j)
        | (Ex #j. (#j < #i) & Reveal( someagent ) @ #j)"


///////////////////// MAIN PROPERTY /////////////////////////////
/// If a key is accepted that is known to the adversary then one of the
/// long term identity keys was known by the adversary
lemma security[hide_lemma=hidden_leafkeysetparts,hide_lemma=hidden_gotleaves]:
    "All #i1 #i2 gid leafkeys gk.
        Accept( gid, leafkeys, gk ) @ #i1 & K( gk ) @ #i2
    ==> Ex #j ik lk otherleaves. (leafkeys = (<'k',ik,lk> + otherleaves)) & Reveal( ik ) @ #j"

end

// vim: ft=spthy
